﻿namespace logowanie
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.w_wynik = new System.Windows.Forms.Label();
            this.send_back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // w_wynik
            // 
            this.w_wynik.AutoSize = true;
            this.w_wynik.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.w_wynik.Location = new System.Drawing.Point(311, 187);
            this.w_wynik.Name = "w_wynik";
            this.w_wynik.Size = new System.Drawing.Size(118, 31);
            this.w_wynik.TabIndex = 1;
            this.w_wynik.Text = "w_wynik";
            // 
            // send_back
            // 
            this.send_back.Location = new System.Drawing.Point(317, 286);
            this.send_back.Name = "send_back";
            this.send_back.Size = new System.Drawing.Size(139, 23);
            this.send_back.TabIndex = 2;
            this.send_back.Text = "Wróć do logowania";
            this.send_back.UseVisualStyleBackColor = true;
            this.send_back.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.send_back);
            this.Controls.Add(this.w_wynik);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label w_wynik;
        private System.Windows.Forms.Button send_back;
    }
}