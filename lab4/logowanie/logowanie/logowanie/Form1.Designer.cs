﻿namespace logowanie
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.login_tekst = new System.Windows.Forms.Label();
            this.haslo_tekst = new System.Windows.Forms.Label();
            this.wynik = new System.Windows.Forms.Label();
            this.login_button = new System.Windows.Forms.Button();
            this.haslo = new System.Windows.Forms.TextBox();
            this.login = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // login_tekst
            // 
            this.login_tekst.AutoSize = true;
            this.login_tekst.Location = new System.Drawing.Point(197, 68);
            this.login_tekst.Name = "login_tekst";
            this.login_tekst.Size = new System.Drawing.Size(33, 13);
            this.login_tekst.TabIndex = 0;
            this.login_tekst.Text = "Login";
            // 
            // haslo_tekst
            // 
            this.haslo_tekst.AutoSize = true;
            this.haslo_tekst.Location = new System.Drawing.Point(197, 142);
            this.haslo_tekst.Name = "haslo_tekst";
            this.haslo_tekst.Size = new System.Drawing.Size(36, 13);
            this.haslo_tekst.TabIndex = 1;
            this.haslo_tekst.Text = "Hasło";
            // 
            // wynik
            // 
            this.wynik.AutoSize = true;
            this.wynik.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.wynik.Location = new System.Drawing.Point(462, 171);
            this.wynik.Name = "wynik";
            this.wynik.Size = new System.Drawing.Size(0, 20);
            this.wynik.TabIndex = 2;
            // 
            // login_button
            // 
            this.login_button.Location = new System.Drawing.Point(238, 232);
            this.login_button.Name = "login_button";
            this.login_button.Size = new System.Drawing.Size(75, 23);
            this.login_button.TabIndex = 3;
            this.login_button.Text = "Zaloguj";
            this.login_button.UseVisualStyleBackColor = true;
            this.login_button.Click += new System.EventHandler(this.login_button_Click);
            // 
            // haslo
            // 
            this.haslo.Location = new System.Drawing.Point(238, 139);
            this.haslo.Name = "haslo";
            this.haslo.Size = new System.Drawing.Size(100, 20);
            this.haslo.TabIndex = 4;
            // 
            // login
            // 
            this.login.Location = new System.Drawing.Point(238, 65);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(100, 20);
            this.login.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.login);
            this.Controls.Add(this.haslo);
            this.Controls.Add(this.login_button);
            this.Controls.Add(this.wynik);
            this.Controls.Add(this.haslo_tekst);
            this.Controls.Add(this.login_tekst);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label login_tekst;
        private System.Windows.Forms.Label haslo_tekst;
        private System.Windows.Forms.Label wynik;
        private System.Windows.Forms.Button login_button;
        private System.Windows.Forms.TextBox haslo;
        private System.Windows.Forms.TextBox login;
    }
}

