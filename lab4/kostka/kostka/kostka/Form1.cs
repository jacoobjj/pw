﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kostka
{
    
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public int Losuj()
        {
            Random rnd = new Random();
            int liczba = rnd.Next(1, 6);
            return liczba;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text = "" + Losuj();
        }

        private void r(object sender, KeyEventArgs e)
        {
            Clipboard.SetText(label1.Text);
        }

        private void label1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            label1.Text = "" + Losuj();
        }
    }
}
