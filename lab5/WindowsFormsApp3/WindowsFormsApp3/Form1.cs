﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Diagnostics;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=komis;";
            string query = "SELECT * FROM samochody";

            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            listBox1.Items.Clear();
            try
            {
                databaseConnection.Open();

                reader = commandDatabase.ExecuteReader();

                if (reader.HasRows)
                {
                    MessageBox.Show("Udało się!");

                    while (reader.Read())
                    {

                        string[] row = { reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5) };
                        listBox1.Items.Add(row[0] + " " + row[1] + " " + row[2] + " " + row[3] + " " + row[4] + " " + row[5]);
                    }
                }
                else
                {
                    MessageBox.Show("Brak rekordów!");
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=komis;";
            string query = "SELECT * FROM samochody ORDER BY marka ASC";

            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            listBox1.Items.Clear();
            try
            {
                databaseConnection.Open();

                reader = commandDatabase.ExecuteReader();

                if (reader.HasRows)
                {
                    MessageBox.Show("Udało się!");

                    while (reader.Read())
                    {

                        string[] row = { reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5) };
                        listBox1.Items.Add(row[0] + " " + row[1] + " " + row[2] + " " + row[3] + " " + row[4] + " " + row[5]);
                    }
                }
                else
                {
                    MessageBox.Show("Brak rekordów!");
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private void button3_Click(object sender, EventArgs e)
        {
            int pojemnosc = Convert.ToInt32(textBox3.Text);
            int cena = Convert.ToInt32(textBox5.Text);
            string append = "'" + textBox1.Text + "', '" + textBox2.Text + "', " + pojemnosc + ", '" + textBox4.Text + "', " + cena + "";
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=komis;";
            try
            {
                using (MySqlConnection con = new MySqlConnection(connectionString))
                {
                    con.Open();
                    using (MySqlCommand command = new MySqlCommand("INSERT INTO samochody (ID, marka, model, pojemnosc, kolor, cena) VALUES (NULL, " + append + ");", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
                MessageBox.Show("Udało się!");
            }
            catch (SystemException ex)
            {
                MessageBox.Show(string.Format("Wystąpił błąd: {0}", ex.Message));
            }
        }



        private void button4_Click(object sender, EventArgs e)
        {
            string ID = textBox6.Text;
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=komis;";
            try
            {
                using (MySqlConnection con = new MySqlConnection(connectionString))
                {
                    con.Open();
                    using (MySqlCommand command = new MySqlCommand("DELETE FROM samochody WHERE samochody.ID = '" + ID + "'", con))
                        {
                            command.ExecuteNonQuery();
                        }
                    con.Close();
                }
                MessageBox.Show("Udało się!");
            }
            catch (SystemException ex)
            {
            MessageBox.Show(string.Format("Wystąpił błąd: {0}", ex.Message));
            }
        }

        
    }
}
