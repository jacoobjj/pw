﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zad1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        private void label4_Click(object sender, EventArgs e)
        {

        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            if((height.Text != "") && (weight.Text != "") && (age.Text != ""))
            {
                int waga = Convert.ToInt32(weight.Text);
                int wiek = Convert.ToInt32(age.Text);
                int wzrost = Convert.ToInt32(height.Text);
                Single bmi;
                if (female.Checked == true)
                {
                    bmi = (float)((9.99 * waga) + (6.25 * wzrost) - (4.92 * wiek) - 161);
                    wynik.Text = bmi.ToString();
                }
                if (male.Checked == true)
                {
                    bmi = (float)((9.99 * waga) + (6.25 * wzrost) - (4.92 * wiek) + 5);
                    wynik.Text = bmi.ToString();
                }
                wynik.ForeColor = Color.Black;
            }
            else
            {
                wynik.ForeColor = Color.Red;
                wynik.Text = "brak danych";
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            height.Text = String.Empty;
            weight.Text = String.Empty;
            age.Text = String.Empty;
            male.Checked = false;
            female.Checked = false;
        }
        private void female_CheckedChanged(object sender, EventArgs e)
        {
            if(male.Checked == true)
            {
                male.Checked = false;
            }
        }
        private void male_CheckedChanged(object sender, EventArgs e)
        {
            if (female.Checked == true)
            {
                female.Checked = false;
            }
            
        }
        
    }
}
