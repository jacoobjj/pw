﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BackgroundWorkerSample
{
    public partial class Form2 : Form
    {
        Form1 setVariables = new Form1();
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            
            maxInRangeBox.Text = setVariables.maxInRange.ToString();
            minInRangeBox.Text = setVariables.minInRange.ToString();
            amountInRangeBox.Text = setVariables.amountInRange.ToString();
            divisiableNumberBox.Text = setVariables.divisibleNumber.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            setVariables.maxInRange = Convert.ToInt32(maxInRangeBox.Text);
            setVariables.minInRange = Convert.ToInt32(minInRangeBox.Text);
            setVariables.amountInRange = Convert.ToInt32(amountInRangeBox.Text);
            setVariables.divisibleNumber = Convert.ToInt32(divisiableNumberBox.Text);
            setVariables.ShowDialog();
            this.Close();
        }
    }
}
