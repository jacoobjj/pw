﻿namespace BackgroundWorkerSample
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.amountInRangeBox = new System.Windows.Forms.TextBox();
            this.divisiableNumberBox = new System.Windows.Forms.MaskedTextBox();
            this.maxInRangeBox = new System.Windows.Forms.TextBox();
            this.minInRangeBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // amountInRangeBox
            // 
            this.amountInRangeBox.Location = new System.Drawing.Point(372, 131);
            this.amountInRangeBox.Name = "amountInRangeBox";
            this.amountInRangeBox.Size = new System.Drawing.Size(100, 20);
            this.amountInRangeBox.TabIndex = 0;
            // 
            // divisiableNumberBox
            // 
            this.divisiableNumberBox.Location = new System.Drawing.Point(372, 157);
            this.divisiableNumberBox.Name = "divisiableNumberBox";
            this.divisiableNumberBox.Size = new System.Drawing.Size(100, 20);
            this.divisiableNumberBox.TabIndex = 1;
            // 
            // maxInRangeBox
            // 
            this.maxInRangeBox.Location = new System.Drawing.Point(372, 211);
            this.maxInRangeBox.Name = "maxInRangeBox";
            this.maxInRangeBox.Size = new System.Drawing.Size(100, 20);
            this.maxInRangeBox.TabIndex = 2;
            // 
            // minInRangeBox
            // 
            this.minInRangeBox.Location = new System.Drawing.Point(372, 185);
            this.minInRangeBox.Name = "minInRangeBox";
            this.minInRangeBox.Size = new System.Drawing.Size(100, 20);
            this.minInRangeBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(274, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Amout";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(274, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Divide by";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(274, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Minimal random";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(274, 214);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Maximal random";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(277, 248);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(195, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Send values";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.minInRangeBox);
            this.Controls.Add(this.maxInRangeBox);
            this.Controls.Add(this.divisiableNumberBox);
            this.Controls.Add(this.amountInRangeBox);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox amountInRangeBox;
        private System.Windows.Forms.MaskedTextBox divisiableNumberBox;
        private System.Windows.Forms.TextBox maxInRangeBox;
        private System.Windows.Forms.TextBox minInRangeBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
    }
}