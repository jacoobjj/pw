#include <windows.h>
#include "zad1.h"
#include "zad2.h"
/* This is where all the input to the window goes to */
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	PAINTSTRUCT ps;
	static int multi = 1;
	static int x_kursora,y_kursora;
	static COLORREF kolor = RGB(0,255,0);
	//static color kolor = 0x0000FF;
	//static int podstawowy = 1;
	HDC hdc;
	HFONT hFont;
	switch(Message) {
		
		//ZADANIE 1
		
		
		
		/*
		
		case WM_PAINT:
			hdc = BeginPaint(hwnd, &ps);
			//zmianaWielkosci(hdc, multi);
			EndPaint(hwnd, &ps);
			break;
		case WM_LBUTTONDOWN:
    	{
        multi++;
        InvalidateRect(hwnd, NULL, TRUE);
        break;
    	}
    	case WM_RBUTTONDOWN:
    	{
        multi--;
        InvalidateRect(hwnd, NULL, TRUE);
        break;
    	}
    	
    	*/
    	
    	
    	
    	
    	//ZADANIE 2
    	
    	case WM_PAINT:
			hdc = BeginPaint(hwnd, &ps);
			//Napis(hdc, hFont, x_kursora, y_kursora);
			SetTextColor(hdc, kolor);
			static LOGFONT lf; /* Deklaracja zmiennej struktury LOGFONT*/
			lf.lfHeight = 50;
			lstrcpy (lf.lfFaceName, TEXT ("Times New Roman CE"));
			hFont = CreateFontIndirect(&lf);
			SelectObject(hdc, hFont);
			TextOut(hdc, x_kursora, y_kursora, "Jakub Jurycz", 12);
			DeleteObject(hFont);
			EndPaint(hwnd,&ps);
			break;
			
		case WM_LBUTTONDOWN:
    	{
        	x_kursora = LOWORD(lParam);
			y_kursora = HIWORD(lParam);
        	InvalidateRect(hwnd, NULL, TRUE);
        	break;
    	}
    	
    	
    	case WM_LBUTTONDBLCLK:
    	{
        	kolor = RGB(0,255,0);
        	InvalidateRect(hwnd, NULL, TRUE);
        	break;
    	}
    	case WM_RBUTTONDBLCLK:
    	{
        	kolor = RGB(255,0,0);
        	InvalidateRect(hwnd, NULL, TRUE);
        	break;
    	}
    	
    	
    	
    	
    	
    	
    	
		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}
		
		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}

/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc; /* A properties struct of our window */
	HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
	MSG msg; /* A temporary location for all messages */

	/* zero out the struct and set the stuff we want to modify */
	memset(&wc,0,sizeof(wc));
	wc.style 		 = CS_DBLCLKS;
	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.lpfnWndProc	 = WndProc; /* This is where we will send messages to */
	wc.hInstance	 = hInstance;
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	
	/* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszClassName = "WindowClass";
	wc.hIcon		 = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION); /* use the name "A" to use the project icon */

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,"WindowClass","Caption",WS_VISIBLE|WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, /* x */
		CW_USEDEFAULT, /* y */
		800, /* width */
		800, /* height */
		NULL,NULL,hInstance,NULL);

	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	/*
		This is the heart of our program where all input is processed and 
		sent to WndProc. Note that GetMessage blocks code flow until it receives something, so
		this loop will not produce unreasonably high CPU usage
	*/
	while(GetMessage(&msg, NULL, 0, 0) > 0) { /* If no error is received... */
		TranslateMessage(&msg); /* Translate key codes to chars if present */
		DispatchMessage(&msg); /* Send it to WndProc */
	}
	return msg.wParam;
}
